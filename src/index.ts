import { UsersTable } from './tables';
import { simpleTableEnhancer, coolTableEnhancer } from './enhancers';

const usersTableObj = new UsersTable();
const simpleEnhancedTableObj = simpleTableEnhancer(usersTableObj);
const coolEnhancedTableObj = coolTableEnhancer(usersTableObj);

// simpleEnhancedTableObj.getNAME();
// coolEnhancedTableObj.getNAME();

// eslint-disable-next-line dot-notation
console.log(`simpleEnhancedTableObj 'hidden' getNAME method returns: ${simpleEnhancedTableObj['getNAME']()}`);
// eslint-disable-next-line dot-notation
console.log(`simpleEnhancedTableObj 'hidden' getID method returns: ${simpleEnhancedTableObj['getID']()}`);

console.log(`coolEnhancedTableObj getNAME method returns: ${coolEnhancedTableObj.getNAME()}`);
console.log(`coolEnhancedTableObj getID method returns: ${coolEnhancedTableObj.getID()}`);
