export class GenericTable {
  __TABLENAME__: string;

  constructor(tableName: string) {
    this.__TABLENAME__ = tableName;
  }
};
