import { GenericTable } from '.';

export class UsersTable extends GenericTable {
  ID: string;
  NAME: string;
  SURNAME: string;
  EMAIL: string;
  
  constructor() {
    super('users');
    // assign the real fields names
    this.ID = 'id';
    this.NAME = 'name';
    this.SURNAME = 'surname';
    this.EMAIL = 'email';
  }
};
