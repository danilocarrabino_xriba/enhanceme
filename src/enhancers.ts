import { GenericTable } from './tables';

export type Get<Type> = {
  [Property in keyof Omit<Type, "__TABLENAME__"> as `get${Capitalize<string & Property>}`]: () => Type[Property]
};
export type EnhancedTable<Type> = Type & Get<Type>;

export function coolTableEnhancer<Type extends GenericTable>(tableObj: Type): EnhancedTable<Type> {
  for (const key of Object.keys(tableObj)) {
    if (key !== '__TABLENAME__') {
      tableObj[`get${key}`] = function(): string {
        return `${tableObj.__TABLENAME__}.${tableObj[`${key}`]}`;
      };
    }
  }
  return tableObj as EnhancedTable<Type>;
}

export function simpleTableEnhancer<Type extends GenericTable>(tableObj: Type): Type {
  for (const key of Object.keys(tableObj)) {
    if (key !== '__TABLENAME__') {
      tableObj[`get${key}`] = function(): string {
        return `${tableObj.__TABLENAME__}.${tableObj[`${key}`]}`;
      };
    }
  }
  return tableObj;
}
