# Introduction
How to enhance an object and its type at runtime in Typescript

# Build and Run

## Build:
> `npm run build`

## Run:
> `npm start`
